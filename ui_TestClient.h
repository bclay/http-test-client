/********************************************************************************
** Form generated from reading UI file 'TestClient.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TESTCLIENT_H
#define UI_TESTCLIENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TestClient
{
public:
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QLineEdit *mServerUrl;
    QPushButton *mSendBtn;
    QGroupBox *groupBox_2;
    QLineEdit *mDataFile;
    QPlainTextEdit *mRequestReply;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *TestClient)
    {
        if (TestClient->objectName().isEmpty())
            TestClient->setObjectName(QString::fromUtf8("TestClient"));
        TestClient->resize(404, 455);
        centralWidget = new QWidget(TestClient);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(30, 20, 341, 51));
        mServerUrl = new QLineEdit(groupBox);
        mServerUrl->setObjectName(QString::fromUtf8("mServerUrl"));
        mServerUrl->setGeometry(QRect(20, 20, 301, 20));
        mSendBtn = new QPushButton(centralWidget);
        mSendBtn->setObjectName(QString::fromUtf8("mSendBtn"));
        mSendBtn->setGeometry(QRect(150, 340, 91, 31));
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(30, 90, 341, 51));
        mDataFile = new QLineEdit(groupBox_2);
        mDataFile->setObjectName(QString::fromUtf8("mDataFile"));
        mDataFile->setGeometry(QRect(20, 20, 301, 20));
        mRequestReply = new QPlainTextEdit(centralWidget);
        mRequestReply->setObjectName(QString::fromUtf8("mRequestReply"));
        mRequestReply->setGeometry(QRect(30, 170, 341, 131));
        TestClient->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(TestClient);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 404, 21));
        TestClient->setMenuBar(menuBar);
        mainToolBar = new QToolBar(TestClient);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        TestClient->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(TestClient);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        TestClient->setStatusBar(statusBar);

        retranslateUi(TestClient);

        QMetaObject::connectSlotsByName(TestClient);
    } // setupUi

    void retranslateUi(QMainWindow *TestClient)
    {
        TestClient->setWindowTitle(QApplication::translate("TestClient", "TestClient", nullptr));
        groupBox->setTitle(QApplication::translate("TestClient", "Server", nullptr));
        mSendBtn->setText(QApplication::translate("TestClient", "Send", nullptr));
        groupBox_2->setTitle(QApplication::translate("TestClient", "File to Send", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TestClient: public Ui_TestClient {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TESTCLIENT_H
