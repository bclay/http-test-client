#include "TestClient.h"
#include "ui_TestClient.h"

TestClient::TestClient(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TestClient)
{
    ui->setupUi(this);

	ui->mServerUrl->setText("http://127.0.0.1:8080");
	ui->mDataFile->setText("When_I_consider_WS.txt");

	mAccessManager = new QNetworkAccessManager(this);

	if (mAccessManager != NULL)
	{
		connect(mAccessManager, SIGNAL(authenticationRequired(QNetworkReply*, QAuthenticator*)),
			SLOT(authenticationRequired(QNetworkReply*, QAuthenticator*)));
		connect(mAccessManager, SIGNAL(proxyAuthenticationRequired(QNetworkProxy, QAuthenticator*)),
			SLOT(proxyAuthenticationRequired(QNetworkProxy, QAuthenticator*)));
	}
}

///////////////////////////////////////////////////////////////////////////////
TestClient::~TestClient()
{
	if (mAccessManager != NULL)
	{
		delete mAccessManager;
	}
    delete ui;
}

///////////////////////////////////////////////////////////////////////////////
void TestClient::authenticationRequired(QNetworkReply *reply,
	QAuthenticator *authenticator)
{

	authenticator->setUser(mServerUsername);
	authenticator->setPassword(mServerPassword);

}

///////////////////////////////////////////////////////////////////////////////
void TestClient::proxyAuthenticationRequired(const QNetworkProxy &proxy,
	QAuthenticator *authenticator)
{
	authenticator->setUser(mServerUsername);
	authenticator->setPassword(mServerPassword);
}

///////////////////////////////////////////////////////////////////////////////
void TestClient::FinishedPost(QNetworkReply *newworkReply)
{
	int status = -1;

	QString postReply = newworkReply->readAll();

	std::string str = postReply.toStdString();

	QVariant statusCode = newworkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
	status = statusCode.toInt();

	QString reason = newworkReply->attribute(
		QNetworkRequest::HttpReasonPhraseAttribute).toString();

	ui->mRequestReply->appendPlainText("Data transfer complete - status: " +
		QString::number(statusCode.toInt()));

}
///////////////////////////////////////////////////////////////////////////////
void TestClient::FinishedRead()
{
	if (mCaptureFile != NULL)
	{
		mCaptureFile->close();

		delete mCaptureFile;

		mCaptureFile = NULL;
	}

	QString postReply = mNetworkReply->readAll();

	QVariant statusCode = mNetworkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
	int status = statusCode.toInt();

	QString reason = mNetworkReply->attribute(
		QNetworkRequest::HttpReasonPhraseAttribute).toString();

	ui->mRequestReply->appendPlainText("Data transfer complete - status: " + reason);
	//								QString::number(statusCode.toInt()));
}

///////////////////////////////////////////////////////////////////////////////
void TestClient::NetworkReadyRead()
{
	char buf[5000];

	qint64 chunkSize;

	QString allbuf;

	while (mNetworkReply->bytesAvailable() > 0)
	{
		chunkSize = mNetworkReply->bytesAvailable();
		if (chunkSize > 4096)
		{
			chunkSize = 4096;
		}

		memset(&buf[0], 0, chunkSize + 1);

		if (chunkSize == mNetworkReply->read(&buf[0], chunkSize))
		{
			if (mCaptureFile != NULL)
			{
				if (mCaptureFile->isOpen())
				{
					mCaptureFile->write(buf, chunkSize);
				}
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
void TestClient::NetworkErrors(QNetworkReply::NetworkError error)
{
	int status = -1;
	QString errString;

//	errString = error.errorString();

	if (mCaptureFile != NULL)
	{
		mCaptureFile->close();

		delete mCaptureFile;

		mCaptureFile = NULL;
	}
	ui->mRequestReply->appendPlainText(" network error: " + QString::number(error));
}

///////////////////////////////////////////////////////////////////////////////
void TestClient::SslErrors(const QList<QSslError> &errors)
{
	int status = -1;

	QList<QSslError> ::iterator errorIter;
	QSslError sslError;
	QString errString;

	for (int index = 0; index < errors.size(); index++)
	{
		sslError = errors[index];

		errString = sslError.errorString();
	}


	status = status;
}
///////////////////////////////////////////////////////////////////////////////
void TestClient::on_mSendBtn_clicked()
{
	int status = -1;

	ui->mRequestReply->appendPlainText("Sending test message");

	if (mAccessManager != NULL)
	{
		QString msgType = "/sendFile";

		QUrl url(ui->mServerUrl->text() + msgType);

		//		url.setUserName(mServerUsername);
		//		url.setPassword(mServerPassword);

		QNetworkRequest request(url);

#ifdef CONFIG_SSL
		if (mServerHost.contains("https") == true)
		{
			QSslConfiguration config = request.sslConfiguration();
			config.setPeerVerifyMode(QSslSocket::VerifyNone);
			config.setProtocol(QSsl::TlsV1_0);
			request.setSslConfiguration(config);
		}
#endif // CONFIG_SSL

		QFile file(ui->mDataFile->text());

		bool opened = file.open(QIODevice::ReadOnly | QIODevice::Text);

		if (opened == true)
		{
			mPostMsgData = file.readAll();

			QNetworkProxyFactory::setUseSystemConfiguration(true);

			request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant(QString("application/json")));

			mNetworkReply = mAccessManager->post(request, mPostMsgData);

			if (mNetworkReply != NULL)
			{
				connect(mNetworkReply, SIGNAL(readyRead()), this, SLOT(NetworkReadyRead()));
				connect(mNetworkReply, SIGNAL(finished()), this, SLOT(FinishedRead()));

				connect(mNetworkReply, SIGNAL(error(QNetworkReply::NetworkError)), this,
					SLOT(NetworkErrors(QNetworkReply::NetworkError)));

				mNetworkReply->ignoreSslErrors();
	//			connect(mNetworkReply, SIGNAL(sslErrors(QList<QSslError>)), this,
	//				SLOT(SslErrors(QList<QSslError>)));

				status = 0;
			}
		}
	}
}

