#ifndef TESTCLIENT_H
#define TESTCLIENT_H

#include <QMainWindow>
#include <QtNetwork/qauthenticator.h>
#include <QNetworkAccessManager>
#include <QtNetwork/QNetworkProxy>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QSslError>

#include <QFile>
#include <QTimer>

namespace Ui {
class TestClient;
}

class TestClient : public QMainWindow
{
    Q_OBJECT

public:
    explicit TestClient(QWidget *parent = nullptr);
    ~TestClient();

private slots:
    void on_mSendBtn_clicked();

	void NetworkReadyRead();

	void FinishedRead();
	void FinishedPost(QNetworkReply *newworkReply);

	void NetworkErrors(QNetworkReply::NetworkError error);

	void SslErrors(const QList<QSslError> &error);

	void authenticationRequired(QNetworkReply *reply, QAuthenticator *auth);
	void proxyAuthenticationRequired(const QNetworkProxy &proxy, QAuthenticator *auth);

private:
    Ui::TestClient *ui;

	QNetworkAccessManager *mAccessManager;
	QNetworkReply *mNetworkReply;

	QFile *mCaptureFile;
	QByteArray mPostMsgData;

	QString mServerUsername;
	QString mServerPassword;

};

#endif // TESTCLIENT_H

